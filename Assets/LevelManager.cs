﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void LoadLevel(string name) {
        Debug.Log("Level 1 loaded");
        SceneManager.LoadScene(name);
    }
    public void QuitLevel()
    {
        Application.Quit();
    }
    public void Return(string name)
    {
        Debug.Log("Level 1 loaded");
        SceneManager.LoadScene(name);
    }

}
